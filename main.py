import pathlib
import csv
import datetime
import jinja2

"""
222
создаем файл с джинджей
"""


def make_weather_html(city, filename):

    city = str(city).capitalize()
    filename = str(filename)

    # имя файла для погодных данных по городу
    city_weather_filename = city + '_weather.html'
    city_weather_path = pathlib.Path.cwd() / city_weather_filename

    # создадим html при помощи шаблонизатора
    environment = jinja2.Environment()
    path_template = pathlib.Path.cwd() / 'weather_template.txt'
    template = environment.from_string(path_template.read_text())
    city_data_generator = get_city_data(city, filename)
    context = {
        "city": city,
        "data_city_sorted": city_data_generator
    }
    with open(city_weather_path, mode="w", encoding="utf-8") as city_weather_file:
        city_weather_file.write(template.render(context))



"""
возврат сортированного списка
"""


def get_city_data(city, filename):

    # получим список словарей погодных данных по городу
    data_file = get_city_data_day(city, filename)  # создаем генератор данных файла
    data_city = []
    for current_data in data_file:
        data_city.append(current_data)
    data_city_sorted = sorted(data_city, key=lambda d: d['day_as_data'])

    return data_city_sorted


"""
генератор строки файла (на самом деле генератор тут не нужен потому что список 
все равно нужно сортировать)
"""


def get_city_data_day(city, filename):

    path_data = pathlib.Path.cwd() / filename

    try:
        with open(path_data, 'r', newline='', encoding='UTF-8') as csvfile:
            reader = csv.DictReader(csvfile, delimiter=';')
            for row in reader:
                if row['town'] == city:
                    # ключ day_as_data необходим для сортировки по дате
                    yield {
                        'day_as_data': datetime.datetime.strptime(row['day'], '%d.%m.%Y'),
                        'day': row['day'],
                        'weather': row['weather']
                    }

    except FileNotFoundError:
        print('Weather data file not found (' + filename + ')')


if __name__ == "__main__":
    make_weather_html('Bataysk', 'weather_data.csv')
    make_weather_html('astana', 'weather_data.csv')
    make_weather_html('moscow', 'weather_data.csv')
    make_weather_html('Seoul', 'weather_data.csv')
    make_weather_html(111, 222)
